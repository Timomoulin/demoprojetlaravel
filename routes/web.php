<?php

use App\Http\Controllers\AdminCategorieController;
use App\Http\Controllers\AdminProduitController;
use App\Http\Controllers\ClientCommandeController;
use App\Http\Controllers\ClientPanierController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\VisiteurProduitController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource("/produits", VisiteurProduitController::class)->only(["index", "show"]);
Route::get("/contact", [Controller::class, "afficheForm"]);
Route::post("/contact", [Controller::class, "traitmentForm"]);
Route::get('/reload-captcha', [Controller::class, 'reloadCaptcha']);

Route::middleware("auth")->group(function () {
    //ICI les routes des personnes logger
    Route::resource("/commandes", ClientCommandeController::class);

    Route::resource("/panier", ClientPanierController::class)->except("create")->parameter("panier", "produit");
    Route::delete("/panier", [ClientPanierController::class, "vider"]);

    Route::get("/testClient", function () {
        return view("client.test");
    });
});



Route::middleware("OnlyAdmin")->group(function () {
    //ICI les routes des admins
    Route::get("/testAdmin", function () {
        return view("admin.test");
    });
    Route::resource("admin/produits", AdminProduitController::class);
    Route::resource("admin/categories", AdminCategorieController::class);
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
