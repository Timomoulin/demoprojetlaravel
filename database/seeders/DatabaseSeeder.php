<?php

namespace Database\Seeders;

use App\Models\Categorie;
use App\Models\Commande;
use App\Models\Ligne;
use App\Models\Produit;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        $roleAdmin = Role::create([
            "nom" => "admin"
        ]);

        $roleClient = Role::create([
            "nom" => "client"
        ]);

        $moi = User::create([
            "name" => "T.Moulin",
            "email" => "timomoulin@msn.com",
            "password" => bcrypt("timomoulin@msn.com1"),
            "role_id" => $roleAdmin->id
        ]);

        $client = User::create([
            "name" => "Mr Client",
            "email" => "client@email.com",
            "password" => bcrypt("client@email.com1"),
            "role_id" => $roleClient->id
        ]);

        $categorie1 = Categorie::create([
            "nom" => "Cours PHP"
        ]);

        $categorie2 = Categorie::create([
            "nom" => "Cours \"JS\""
        ]);

        $produit1 = Produit::create([
            "nom" => "Les variables PHP",
            "prix" => 10,
            "description" => "Introduction aux variables en PHP, déclaration de vairaible en PHP",
            "categorie_id" => $categorie1->id
        ]);

        $categories = Categorie::all();
        foreach ($categories as $uneCategorie) {
            Produit::factory(10)->create([
                "categorie_id" => $uneCategorie->id
            ]);
        }

        $commande1 = Commande::create([
            "etat" => "en preparation",
            "user_id" => $moi->id
        ]);

        $ligne1 = Ligne::create([
            "commande_id" => $commande1->id,
            "produit_id" => $produit1->id,
            "quantite" => 5
        ]);

        $ligne2 = Ligne::create([
            "commande_id" => $commande1->id,
            "produit_id" => 2,
            "quantite" => 2
        ]);
    }
}
