<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lignes', function (Blueprint $table) {
            $table->foreignId("commande_id")->constrained("commandes")->onDelete("cascade");
            $table->foreignId("produit_id")->constrained("produits")->onDelete("cascade");
            $table->integer("quantite");
            $table->primary(["commande_id", "produit_id"]);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lignes');
    }
};
