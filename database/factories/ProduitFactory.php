<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Produit>
 */
class ProduitFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            "nom" => $this->faker->words(3, true),
            "prix" => $this->faker->numberBetween(5, 50),
            "description" => $this->faker->text(200),
            //"image" => $this->faker->imageUrl()
        ];
    }
}
