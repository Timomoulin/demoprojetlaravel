<?php

namespace App\Http\Controllers;

use App\Models\Produit;
use Darryldecode\Cart\Facades\CartFacade;
use Illuminate\Http\Request;

class ClientPanierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $panier = CartFacade::getContent();

        return view("client.panier.index", ["panier" => $panier]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Produit  $produit
     * @return \Illuminate\Http\Response
     */
    public function show(Produit $produit)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Produit  $produit
     * @return \Illuminate\Http\Response
     */
    public function edit(Produit $produit)
    {
        $ligne = CartFacade::get($produit->id);
        return view("client.panier.edit", ["ligne" => $ligne]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Produit  $produit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Produit $produit)
    {
        $request->validate(["quantite" => "required|numeric|min:0"]);
        //Le produit est deja dans le panier
        if (CartFacade::get($produit->id)) {
            CartFacade::update($produit->id, array(
                'id' => $produit->id, // inique row ID
                'name' => $produit->nom,
                'price' => $produit->prix,
                'quantity' => array(
                    'relative' => false,
                    'value' => $request->input("quantite")
                ),
                'attributes' => array()
            ));
        } else {
            CartFacade::add(array(
                'id' => $produit->id, // inique row ID
                'name' => $produit->nom,
                'price' => $produit->prix,
                'quantity' => $request->input("quantite"),
                'attributes' => array()
            ));
        }
        session()->flash("success", "Le produit est ajouter au panier");
        return redirect("/produits");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Produit  $produit
     * @return \Illuminate\Http\Response
     */
    public function destroy(Produit $produit)
    {
        CartFacade::remove($produit->id);
        session()->flash("success", "Le produit est retirer du panier");
        return redirect("/panier");
    }

    public function vider()
    {
        CartFacade::clear();
        session()->flash("success", "Le panier est vide");
        return redirect("/panier");
    }
}
