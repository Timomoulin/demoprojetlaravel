<?php

namespace App\Http\Controllers;

use App\Models\Categorie;
use App\Models\Produit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class AdminProduitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produits = Produit::all();
        return view("admin.produits.index", ["lesProduits" => $produits]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lesCategories = Categorie::all();
        return view("admin.produits.create", ["lesCategories" => $lesCategories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributs = $request->validate([
            "nom" => "required|string|min:2|max:255",
            "description" => "required|string|min:2|max:255",
            "prix" => "numeric|min:0|required",
            "categorie_id" => "required|exists:categories,id",
            "image" => "image"
        ]);

        // dd($request->file("image"));

        $cheminDeLImage = $request->file("image")->store("/produits");
        $attributs["image"] = $cheminDeLImage;

        Produit::create($attributs);
        session()->flash("success", "Ajout du produit reusi");
        return redirect("/admin/produits");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Produit  $produit
     * @return \Illuminate\Http\Response
     */
    public function show(Produit $produit)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Produit  $produit
     * @return \Illuminate\Http\Response
     */
    public function edit(Produit $produit)
    {
        $lesCategories = Categorie::all();
        return view("admin.produits.edit", ["lesCategories" => $lesCategories, "produit" => $produit]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Produit  $produit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Produit $produit)
    {
        $attributs = $request->validate([
            "nom" => "required|string|min:2|max:255",
            "description" => "required|string|min:2|max:255",
            "prix" => "numeric|min:0|required",
            "categorie_id" => "required|exists:categories,id",
            "image" => "image"
        ]);

        if ($request->file("image")) {
            $cheminDeLImage = $request->file("image")->store("/produits");
            $attributs["image"] = $cheminDeLImage;
        }

        $produit->update($attributs);
        session()->flash("success", "Modification du produit reusi");
        return redirect("/admin/produits");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Produit  $produit
     * @return \Illuminate\Http\Response
     */
    public function destroy(Produit $produit)
    {
        $cheminImage = $produit->image;

        $resultat = $produit->delete();
        if ($resultat == true) {
            unlink( storage_path("app/public/".$cheminImage));
            session()->flash("success", "Le produit est supprimer");
        }
        session()->flash("success", "Suppresion du produit reusi");
        return redirect("/admin/produits");
    }
}
