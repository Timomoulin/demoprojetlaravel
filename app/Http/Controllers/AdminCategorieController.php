<?php

namespace App\Http\Controllers;

use App\Models\Categorie;
use Illuminate\Http\Request;

class AdminCategorieController extends Controller
{
    /**
     * Display a listing of the resource.
     *@endpoint GET /admin/categories
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lesCategories = Categorie::all();
        return view("admin.categories.index", ["lesCategories" => $lesCategories]);
    }

    /**
     * Show the form for creating a new resource.
     *@endpoint GET /admin/categories/create
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.categories.create");
    }

    /**
     * Store a newly created resource in storage.
     *@endpoint POST /admin/categories
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //La validation
        $attributs = $request->validate([
            "nom" => "required|unique:categories,nom|min:2|max:255|string"
        ]);

        $categorie = Categorie::create($attributs);
        session()->flash("success", "La categorie " . $categorie->nom . " a été ajouter.");
        return redirect("/admin/categories");
    }

    /**
     * Display the specified resource.
     *@endpoint GET /admin/categories/5
     * @param  \App\Models\Categorie  $categorie
     * @return \Illuminate\Http\Response
     */
    public function show(Categorie $category)
    {
        return view("admin.categories.show", ["categorie" => $category]);
    }

    /**
     * Show the form for editing the specified resource.
     *@endpoint GET /admin/categories/5/edit
     * @param  \App\Models\Categorie  $categorie
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Categorie $category)
    {

        return view("admin.categories.edit", ["categorie" => $category]);
    }

    /**
     * Update the specified resource in storage.
     *@endpoint PUT /admin/categories
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Categorie  $categorie
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Categorie $category)
    {
        $attributs = $request->validate([
            "nom" => "string|required|unique:categories,nom," . $category->id
        ]);

        $category->update($attributs);
        session()->flash("success", "La categorie " . $category->nom . " a était modifier .");
        return redirect("/admin/categories");
    }

    /**
     * Remove the specified resource from storage.
     *@endpoint DELETE /admin/categories/5
     * @param  \App\Models\Categorie  $categorie
     * @return \Illuminate\Http\Response
     */
    public function destroy(Categorie $category)
    {
        $resultat = $category->delete();
        if ($resultat == true) {
            session()->flash("success", "La categorie est supprimer");
        }
        return redirect("/admin/categories");
    }
}
