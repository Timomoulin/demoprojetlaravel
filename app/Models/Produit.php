<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produit extends Model
{
    use HasFactory;
    protected $fillable = ["nom", "description", "prix", "categorie_id", "image"];

    public function categorie()
    {
        return $this->belongsTo(Categorie::class, "categorie_id");
    }

    public function lignes()
    {
        return $this->belongsToMany(Commande::class, "lignes")->using(Ligne::class)->withPivot(["quantite"])->withTimestamps();
    }
}
