<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Contact extends Mailable
{
    use Queueable, SerializesModels;

    protected $email;
    protected $sujet;
    protected $corps;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($unEmail, $unSujet, $unMessage)
    {
        $this->email = $unEmail;
        $this->sujet = $unSujet;
        $this->corps = $unMessage;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->subject("Message de $this->email")->view('email.contact', [
            "email" => $this->email,
            "sujet" => $this->sujet,
            "corps" => $this->corps

        ]);
    }
}
