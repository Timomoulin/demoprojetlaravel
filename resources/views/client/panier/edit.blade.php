@extends('layouts.app')
@section('titre')
    Modification du panier
@endsection
@section('content')
    <div class="container">
        <div class="col-12 col-sm-10 col-md-6 col-lg-4 mx-auto">
            <h1 class="my-1">Modification du panier</h1>
            <form action="/panier/{{ $ligne->id }}" method="post">
                @csrf
                @method('put')
                <h3>
                    {{ $ligne->name }}
                </h3>
                <div class='row mb-2'>
                    <label for='prix'>Prix Unitaire </label>
                    <input value='{{ $ligne->price }}' readonly required type='number' class="form-control" id="prix">
                </div>

                <div class='row mb-2'>
                    <label for='quantite'>quantite *</label>
                    <input value='{{ old('quantite') ?? $ligne->quantity }}' name='quantite' required type='number'
                        class="form-control" id="quantite" placeholder="Enter quantite">
                    @error('quantite')
                        <div class='alert alert-danger mt-1'>{{ $message }}</div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection
