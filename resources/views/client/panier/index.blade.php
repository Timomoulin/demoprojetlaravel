@extends('layouts.app')
@section('titre')
    Mon Panier
@endsection
@section('content')
    <h1>Mon Panier</h1>
    <table class="table table-responsive table-striped">
        <thead>
            <th>Nom</th>
            <th>Prix unitaire</th>
            <th>Quantite</th>
            <th>Sous Total</th>
            <th>Actions</th>
        </thead>
        <tbody>
            @foreach ($panier as $ligne)
                <tr>
                    <td>{{ $ligne->name }}</td>
                    <td>{{ $ligne->price }}</td>
                    <td>{{ $ligne->quantity }}</td>
                    <td>{{ $ligne->quantity * $ligne->price }}</td>
                    <td>
                        <a href="/panier/{{ $ligne->id }}/edit" class="btn btn-secondary">Modifier</a>

                        <form action="/panier/{{ $ligne->id }}" method="post">
                            @method('delete')
                            @csrf
                            <button class="btn btn-danger">Supprimer</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            <tr>
                <td>_</td>
                <td>_</td>
                <td>_</td>
                <td>_</td>
                <td>_</td>
            </tr>
        </tbody>
    </table>
    <form action="/commandes" method="post">
        @csrf

        <button class="btn btn-success">Valider le panier</button>
    </form>
    <form action="/panier" method="post" class="mt-2">
        @csrf
        @method('delete')
        <button class="btn btn-danger">Vider le panier</button>
    </form>
@endsection
