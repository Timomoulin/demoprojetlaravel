@extends('layouts.app')
@section('titre')
    Les produits
@endsection
@section('content')
    <h1>Les Produits</h1>
    {{ $lesProduits->links() }}
    <div class="row row-cols-3 my-2">
        @foreach ($lesProduits as $unProduit)
            <div class="card mx-auto my-2" style="width: 18rem;">
                <img src="{{ asset('/storage/' . ($unProduit->image ?? 'produits/default.jpg')) }}" class="card-img-top"
                    alt="...">
                <div class="card-body">
                    <h5 class="card-title">{{ Str::ucfirst($unProduit->nom) }}</h5>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                        card's content.</p>
                    <a href="/produits/{{ $unProduit->id }}" class="btn btn-primary">Go somewhere</a>
                </div>
            </div>
        @endforeach
    </div>
    {{ $lesProduits->links() }}
@endsection
