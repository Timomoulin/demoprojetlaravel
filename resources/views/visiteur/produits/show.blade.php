@extends('layouts.app')
@section('titre')
    {{ $produit->nom }}
@endsection
@section('content')
    <h1>{{ Str::ucfirst($produit->nom) }}</h1>
    <section>
        {{-- TODO Afficher les informations du produit --}}
    </section>

    <section>
        @guest
            <div class="alert alert-warning">
                Pour ajouter l'aticle a votre panier il faut vous <a href="/login"> conecter</a> <br>
                Ou bien vous <a href="/register"> inscrire </a>
            </div>
        @endguest
        @auth
            <div class="container">
                <div class="col-12 col-sm-10 col-md-6 col-lg-4 mx-auto">
                    <h2 class="my-1">
                    </h2>
                    <form action="/panier/{{ $produit->id }}" method="post">
                        @csrf
                        @method('put')
                        <div class='row mb-2'>
                            <label for='quantite'>Quantite *</label>
                            <input value='{{ old('quantite') }}' min="1" name='quantite' required type='number'
                                class="form-control" id="quantite" placeholder="Saisir la quantite">
                            @error('quantite')
                                <div class='alert alert-danger mt-1'>{{ $message }}</div>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        @endauth
    </section>
    <section>
        {{-- TODO Afficher les commentaires et un formulaire d'ajout pour un commentaire --}}
    </section>
@endsection
