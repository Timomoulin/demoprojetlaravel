@extends('layouts.app')
@section('titre')
    Formulaire de contact
@endsection
@section('content')
    <div class="container">
        <div class="col-12 col-sm-10 col-md-6 col-lg-4 mx-auto">
            <h1 class="my-1">Fomulaire de contact</h1>
            <form action="/contact" method="post">
                @csrf

                <div class='row mb-2'>
                    <label for='email'>Email *</label>
                    <input value='{{ old('email') }}' name='email' required type='email' class="form-control"
                        id="email" placeholder="Enter email">
                    @error('email')
                        <div class='alert alert-danger mt-1'>{{ $message }}</div>
                    @enderror
                </div>

                <div class='row mb-2'>
                    <label for='sujet'>sujet *</label>
                    <input value='{{ old('sujet') }}' name='sujet' required type='text' class="form-control"
                        id="sujet" placeholder="Enter sujet">
                    @error('sujet')
                        <div class='alert alert-danger mt-1'>{{ $message }}</div>
                    @enderror
                </div>

                <div class='row mb-2'>
                    <label for='message'>message *</label>
                    <textarea name='message' required class="form-control" id="message" placeholder="Enter message"></textarea>
                    @error('message')
                        <div class='alert alert-danger mt-1'>{{ $message }}</div>
                    @enderror
                </div>

                <div class="row mb-2">
                    <div class="captcha">
                        <span>{!! captcha_img() !!}</span>
                        <button type="button" class="btn btn-danger" class="reload" id="reload">
                            &#x21bb;
                        </button>
                    </div>
                </div>
                <div class="form-group mb-4">
                    <input id="captcha" type="text" class="form-control" placeholder="Enter Captcha" name="captcha">
                </div>
        </div>
        <button type="submit" class="btn btn-primary">Envoyer</button>
        </form>
    </div>
    </div>
@endsection

@section('scriptes')
    <script type="text/javascript">
        $('#reload').click(function() {
            $.ajax({
                type: 'GET',
                url: 'reload-captcha',
                success: function(data) {
                    $(".captcha span").html(data.captcha);
                }
            });
        });
    </script>
@endsection
