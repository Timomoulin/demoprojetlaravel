@extends('layouts.app')
@section('titre')
    Modifier une categorie
@endsection
@section('content')
    <div class="container">
        <div class="col-12 col-sm-10 col-md-6 col-lg-4 mx-auto">
            <h1 class="my-1">Modifier d'une categorie</h1>
            <form action="/admin/categories/{{ $categorie->id }}" method="post">
                @csrf
                @method('put')
                <div class='row mb-2'>
                    <label for='nom'>Nom *</label>
                    <input value='{{ old('nom') ?? $categorie->nom }}' name='nom' required type='text'
                        class="form-control" id="nom" placeholder="Saisir un nom">
                    @error('nom')
                        <div class='alert alert-danger mt-1'>{{ $message }}</div>
                    @enderror
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection
