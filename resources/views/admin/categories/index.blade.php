@extends('layouts.app')
@section('titre')
    Categorie
@endsection
@section('content')
    <h1>Les categorie</h1>
    <a href="/admin/categories/create" class="btn btn-success">Ajouter</a>
    <table class="table table-responsive table-striped">
        <thead>
            <th>Id</th>
            <th>Nom</th>
            <th>Actions</th>
        </thead>
        <tbody>
            @foreach ($lesCategories as $categorie)
                <tr>
                    <td>{{ $categorie->id }}</td>
                    <td>{{ $categorie->nom }}</td>
                    <td>
                        <a href="/admin/categories/{{ $categorie->id }}/edit" class="btn btn-secondary">Modifier</a>
                        <a class="btn btn-primary" href="/admin/categories/{{ $categorie->id }}">Consulter</a>
                        <form class="supform" action="/admin/categories/{{ $categorie->id }}" method="post">
                            @method('delete')
                            @csrf
                            <button class="btn btn-danger">Supprimer</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection

@section('scriptes')
    <script>
        let lesFormulaires = document.querySelectorAll(".supform");

        for (const unFormulaire of lesFormulaires) {
            unFormulaire.addEventListener("submit", function(event) {

                if (confirm("Supprimer la categorie ?") == false) {
                    event.preventDefault();
                    return false;
                }
            })
        }
    </script>
@endsection
