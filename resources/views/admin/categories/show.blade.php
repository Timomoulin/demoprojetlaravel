@extends('layouts.app')
@section('titre')
    {{ $categorie->nom }}
@endsection
@section('content')
    <h1>{{ Str::ucfirst($categorie->nom) }}</h1>
    <div class="container">
        <ul>
            @foreach ($categorie->produits as $unProduit)
                <li>Nom : {{ $unProduit->nom }} Prix : {{ $unProduit->prix }}</li>
            @endforeach

        </ul>
    </div>
@endsection
