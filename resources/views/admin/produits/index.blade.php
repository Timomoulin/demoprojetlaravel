@extends('layouts.app')
@section('titre')
    produit
@endsection
@section('content')
    <h1>Les produit</h1>
    <a href="/admin/produits/create" class="btn btn-success">Ajouter</a>
    <table class="table table-responsive table-striped">
        <thead>
            <th>Id</th>
            <th>Nom</th>
            <th>Prix</th>
            <th>Image</th>
            <th>Categorie</th>
            <th>Actions</th>
        </thead>
        <tbody>
            @foreach ($lesProduits as $produit)
                <tr>
                    <td>{{ $produit->id }}</td>
                    <td>{{ $produit->nom }}</td>
                    <td>{{ $produit->prix }}</td>
                    <td><img src="{{ asset('/storage/' . ($produit->image ?? 'produits/default.jpg')) }}" alt=""> </td>
                    <td>{{ $produit->categorie->nom }}</td>
                    <td>
                        <a href="/admin/produits/{{ $produit->id }}/edit" class="btn btn-secondary">Modifier</a>
                        <a href="/admin/produits/{{ $produit->id }}">Consulter</a>
                        <form action="/admin/produits/{{ $produit->id }}" method="post">
                            @method('delete')
                            @csrf
                            <button class="btn btn-danger">Supprimer</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
