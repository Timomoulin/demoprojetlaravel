@extends('layouts.app')
@section('titre')
    Modification du Produit
@endsection
@section('content')
    <div class="container">
        <div class="col-12 col-sm-10 col-md-6 col-lg-4 mx-auto">
            <h1 class="my-1">Modification d'un Produit</h1>
            <form action="/admin/produits/{{ $produit->id }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class='row mb-2'>
                    <label for='nom'>Nom *</label>
                    <input value='{{ old('nom') ?? $produit->nom }}' name='nom' required type='text' class="form-control"
                        id="nom" placeholder="Saisir le nom">
                    @error('nom')
                        <div class='alert alert-danger mt-1'>{{ $message }}</div>
                    @enderror
                </div>

                <div class='row mb-2'>
                    <label for='prix'>Prix *</label>
                    <input value='{{ old('prix') ?? $produit->prix }}' name='prix' required type='number'
                        class="form-control" id="prix" placeholder="Saisir le prix">
                    @error('prix')
                        <div class='alert alert-danger mt-1'>{{ $message }}</div>
                    @enderror
                </div>

                <div class='row mb-2'>
                    <label for='description'>Description *</label>
                    <input value='{{ old('description') ?? $produit->description }}' name='description' required
                        type='text' class="form-control" id="description" placeholder="Enter description">
                    @error('description')
                        <div class='alert alert-danger mt-1'>{{ $message }}</div>
                    @enderror
                </div>

                <div class="row mb-2">
                    <label for="categorie_id">categorie</label>
                    <select value="{{ old('categorie_id') }}" class="form-control" name="categorie_id" id="categorie_id">
                        <option value="" disabled>Choisir un categorie</option>
                        @foreach ($lesCategories as $uneCategorie)
                            <option {{ $produit->categorie_id == $uneCategorie->id ? 'selected' : '' }}
                                value="{{ $uneCategorie->id }}">
                                {{ Str::ucfirst($uneCategorie->nom) }}</option>
                        @endforeach
                    </select>
                    @error('categorie_id')
                        <div class="alert alert-danger my-2">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class='row mb-2'>
                    <label for='image'>Image </label>
                    <input value='{{ old('image') }}' name='image' type='file' class="form-control" id="image"
                        placeholder="Enter image">
                    @error('image')
                        <div class='alert alert-danger mt-1'>{{ $message }}</div>
                    @enderror
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection
