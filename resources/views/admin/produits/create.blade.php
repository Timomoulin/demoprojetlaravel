@extends('layouts.app')
@section('titre')
    Ajout d'un Produit
@endsection
@section('content')
    <div class="container">
        <div class="col-12 col-sm-10 col-md-6 col-lg-4 mx-auto">
            <h1 class="my-1">Ajout d'un Produit</h1>
            <form action="/admin/produits" method="post" enctype="multipart/form-data">
                @csrf

                <div class='row mb-2'>
                    <label for='nom'>Nom *</label>
                    <input value='{{ old('nom') }}' name='nom' required type='text' class="form-control" id="nom"
                        placeholder="Saisir le nom">
                    @error('nom')
                        <div class='alert alert-danger mt-1'>{{ $message }}</div>
                    @enderror
                </div>

                <div class='row mb-2'>
                    <label for='prix'>Prix *</label>
                    <input value='{{ old('prix') }}' name='prix' required type='number' class="form-control" id="prix"
                        placeholder="Saisir le prix">
                    @error('prix')
                        <div class='alert alert-danger mt-1'>{{ $message }}</div>
                    @enderror
                </div>

                <div class='row mb-2'>
                    <label for='description'>Description *</label>
                    <input value='{{ old('description') }}' name='description' required type='text' class="form-control"
                        id="description" placeholder="Enter description">
                    @error('description')
                        <div class='alert alert-danger mt-1'>{{ $message }}</div>
                    @enderror
                </div>

                <div class="row mb-2">
                    <label for="categorie_id">categorie</label>
                    <select value="{{ old('categorie_id') }}" class="form-control" name="categorie_id" id="categorie_id">
                        <option value="" selected disabled>Choisir un categorie</option>
                        @foreach ($lesCategories as $uneCategorie)
                            <option value="{{ $uneCategorie->id }}">{{ Str::ucfirst($uneCategorie->nom) }}</option>
                        @endforeach
                    </select>
                    @error('categorie_id')
                        <div class="alert alert-danger my-2">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class='row mb-2'>
                    <label for='image'>Image *</label>
                    <input value='{{ old('image') }}' name='image' required type='file' class="form-control" id="image"
                        placeholder="Enter image">
                    @error('image')
                        <div class='alert alert-danger mt-1'>{{ $message }}</div>
                    @enderror
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection
